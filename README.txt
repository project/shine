# siarashield CAPTCHA for Drupal

The siarashield module uses the CAPTCHA web service to
improve the CAPTCHA system and protect email addresses. For
more information on what siarashield is, please visit:
    https://www.cybersiara.com/

## CONFIGURATION

1. Enable siarashield CAPTCHA modules in:
       admin/modules

2. You'll now find a siarashield tab in the CAPTCHA
   administration page available at:
       admin/config/people/captcha/siarashield

3. Register your web site at
       https://mycybersiara.com/

4. Input the site and private keys into the siarashield CAPTCHA settings.

5. Visit the Captcha administration page and set where you
   want the siarashield form to be presented:
       admin/config/people/captcha/siarashield

## KNOWN ISSUES

- cURL requests fail because of outdated root certificate. The reCAPTCHA module
  may not able to connect to Google servers and fails to verify the answer.

  See https://www.drupal.org/node/2481341 for more information.


## THANK YOU

 * Thank you goes to the cybersiara team for all their
   help, support and their amazing Captcha solution
       https://www.cybersiara.com/
