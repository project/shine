(function($) {
      // You can init map here.

      jQuery(document).ready( function() {
        if($("div").hasClass("SiaraShield")){

          $('head').append('<link rel="stylesheet" href="https://embed.mycybersiara.com/CaptchaFormate/CaptchaResources.css" type="text/css" />');
          $('<script />', { type : 'text/javascript', src : 'https://embed.mycybersiara.com/CaptchaFormate/CaptchaResources_wp.js'}).appendTo('head');

          setTimeout(function (){
            $(function () {
              var PublicKey='TEST-CYBERSIARA';
              InitCaptcha(PublicKey);
              $('.CaptchaSubmit').click(function (){
                if(CheckCaptcha()){
                console.log(CyberSiaraToken);
                // Please write your submit button click event function here //
                }
              });
            });
          },1000);
        }
	    });
})(jQuery);
